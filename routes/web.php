<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Routes To Send SMS with CloudsZone
// Route::get('/cloudszone', function () { return view('cloudszone'); })->name('cloudszone');
Route::post('send',   [App\Http\Controllers\SenderController::class, 'sendcloud'])->name('send');

// Routes To Send SMS with ModernCurtain
// Route::get('/moderncurtain', function () { return view('moderncurtain'); })->name('moderncurtain');
// Route::post('sendcurtain',   [App\Http\Controllers\SenderController::class, 'sendcurtain'])->name('sendcurtain');

Route::get('logout',   [App\Http\Controllers\HomeController::class, 'logout'])->name('logout');


