@extends('layouts.logout')
@section('title','welcome cloudszone ')
<!-- Contact Section -->

<Html lang="en">
    <style>
        #digital-clock {
  /* width: 80%; */
  /* font-size: 20px; */
  /* text-align: center; */
  /* font-size: 19vw; */
  /* color: white; */
}
.red-dot {
  /* color: red; */
}
        </style>
<br xmlns="http://www.w3.org/1999/html">
<section class="flexbox-container">
    <div class="col-lg-12 col-md-12 d-flex align-items-center justify-content-center">
        <div class="col-lg-8 col-md-8 box-shadow-2 p-0">
            <div class="card border-grey border-lighten-3 m-0">
                <div class="card-header border-0">
                    <div class="card-title text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="card-body">
                                    @include('alerts.error')
                                    @include('alerts.success')
                                    <section id="contact">
                                       <div class="container">
                                            <div class="row">
                                                    @isset($last_message)
                                                        <div class="alert alert-info  alert-dismissible fade show">
                                                            <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                                                            <h5 style= "font-family: 'Cairo', sans-serif;">رصيدك الحالي:
                                                                {{$last_message->current_user_points}}
                                                            </h5>
                                                        </div>
                                                    @endisset
                                                <div class="col-lg-12 text-center alert-dismissible fade show" >
                                                    <h3 style= "font-family: 'Cairo', sans-serif;">إرسال الرسائل بواسطة منصة منطقة السحاب</h3>
                                                    <hr class="star-primary">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-lg-offset-2">
                                                    <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                                                    <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                                                    <form name="sentMessage" id="contactForm"  METHOD="POST" action="{{route('send')}}">
                                                        @csrf
                                                        <!-- Line test<form name="sentMessage" id="contactForm" method="post" action="http://samuel.net46.net/toExternal/contact_me.php"> -->
                                                        <div class="row control-group ">
                                                            <div  class=" col-lg-3  form-group">
                                                                <h5 style= "font-family: 'Cairo', sans-serif;"> الاســم</h5>
                                                            </div>
                                                            <div class=" col-lg-6 col-md-12 form-group">
                                                                <select class="form-control" id="" name="sender_name_id">
                                                                    <option>اختر اسم المرسل من فضلك </option>
                                                                    @foreach($senders_names as $one)
                                                                    <option value="{{ $one->id }}"
                                                                    @if (old('sender_name_id') == $one->id) selected="selected" @endif>
                                                                    {{ $one->name }}
                                                                    </option>
                                                                @endforeach
                                                                </select>
                                                               
                                                            </div>
                                                            <div  class="col-lg-3 form-group" > 
                                                                <label style="font-size:0.8em;">  
                                                                    
                                                                </label>    
                                                            </div>

                                                            <div  class=" col-lg-3  form-group">
                                                                <h5 style= "font-family: 'Cairo', sans-serif;">أرقام الجوالات</h4>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <textarea rows="6" class="form-control" dir="ltr" title="الارقام المراد الارسال لها " name="mobiles[]" id="mobiles" required data-validation-required-message="Please enter  mobile numbers." ></textarea>
                                                                    <p class="help-block text-danger"></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row container"> 
                                                             <label style="font-size:0.8em;">  الإرشادات </label>
                                                             <label style="font-size:0.7em;" >إضافة الأرقام مباشرةً
                                                             وكل الأرقام تعمل ضمن المملكة العربية السعودية                                                                       </li>
                                                             ويرجي كتابة كل رقم في سطر منفرد</label>
                                                        </div>
                                                        <br>
                                                           
                                                        <div class="row">
                                                            <div  class=" col-lg-3  form-group">
                                                                <h5 style= "font-family: 'Cairo', sans-serif;">رِسـَالتك</h5>
                                                            </div>
                                                            <div class="col-lg-6  ">
                                                                <div class="form-group">
                                                                    <textarea rows="6" class="form-control" dir="ltr"  title="رسالتك" name="message" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                                                    <p class="help-block text-danger"></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label style="font-size:0.8em;">
                                                                        الإرشادات </label>  
                                                            <label style="font-size:0.8em;">تحسب نقطة واحدة للرسالة باللغة الانجليزية، حيث الرسالة 160 حرف، أكثر من ذلك تحسب كل 153 حرف نقطة واحدة.
                                                            أيضاً تحسب نقطة واحدة للرسالة باللغة العربية، حيث الرسالة الواحدة  70 حرف، أكثر من ذلك تحسب كل 67 حرف نقطة.
                                                            وأقصي عدد من الرسائل المتصلة "عدد مقاطع الرسالة الواحدة" هو 8 نقاط<label>
                                                        </div>
                                                        <br>
                                                        
                                                        </div>
                                                        <br>
                                                        <!-- <div id="success"></div> -->
                                                        
                                                        <div class="row">
                                                                <div class="form-group col-xs-12">
                                                                    <p>الساعة الان بتوقيتك المحلي:</p>
                                                                <div id='digital-clock'>
                                                                    <span id='hour'></span><span class='red-dot'>:</span><span id='min'></span><span class='red-dot'>:</span><span id='second'></span>
                                                                    <span id='ampm'></span>
                                                                </div>
                                                                </div>
                                                        </div>

                                                            <div class="row">
                                                                <div class="form-group col-xs-12">
                                                                    <button type="submit" name="submit" class="btn btn-info btn-lg">إرسال</button>
                                                                    <button type="reset" class="btn btn-info btn-lg">إعادة تعيين</button>

                                                                </div>
                                                            </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function showclock() {
        let today = new Date();
        let hours = today.getHours();
        let mins = today.getMinutes();
        let seconds = today.getSeconds();
        var newformat = hours >= 12 ? 'مساءً' : 'صباحاً'; 
        hours = (hours % 12) || 12;
        const addZero = num => {
            if(num < 10) { return '0' + num };
            return num;
        }
        $('#hour').text(addZero(hours));
        $('#min').text(addZero(mins));
        $('#second').text(addZero(seconds));
        $('#ampm').text(newformat);

        }
    setInterval(showclock, 1000);
</script>
</Html>
