<nav class="header-navbar navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow">
        <ul class="nav">
            <li class="nav-item">
                <div class="nav-link" href="#" >
                    <span class="avatar avatar-online">
                        <img  style="height: 35px; width: 43px;" src="{{asset('assets/admin/1.png')}}" alt="">
                    </span>
                    <span style="color:white; font-size:15px;"> &nbsp; مرحبا  
                        <span class="user-name text-bold-700"> {{Auth::user()->name}} </span>
                    </span> 
                </div>
            </li> 
        </ul>
        <ul class="nav justify-content-center">
            <li class="nav-item"> 
                <div class="nav-link"  >
                    <span style="color:white;">
                        <a class="btn btn-outline-light user-name text-bold-700" href="{{route('logout')}}"> <i class="ft-power" style="color:white;"></i>
                        تسجيل الخروج
                        </a>
                    </span> 
                    
        </button>
                </div>
            </li> 
        </ul>
</nav>
