<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted ">
  <!-- Section: Social media -->
  <section
    class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom container"
  >
    <!-- Left -->
    <div class="me-5 d-none d-lg-block">
      <!-- <span>Get connected with us on social networks:</span> -->
    </div>
    <!-- Left -->

    <!-- Right -->
    <div>
      <a href="" class="me-4 text-reset"> 
        <i class="fab fa-facebook"></i>
        
      </a>
      <a href="" class="me-4 text-reset">
        <i class="fab fa-twitter"></i>
      </a>
      <a href="" class="me-4 text-reset">
        <i class="fab fa-google"></i>
      </a>
      <a href="" class="me-4 text-reset">
        <i class="fab fa-instagram"></i>
      </a>
      <a href="" class="me-4 text-reset">
        <i class="fab fa-linkedin"></i>
      </a>
      <a href="" class="me-4 text-reset">
        <i class="fab fa-github"></i>
      </a>
    </div>
    <!-- Right -->
  </section>
  <!-- Section: Social media -->

  <!-- Section: Links  -->
  <section class="container">
    <div class="container text-center text-md-start mt-5">
      <!-- Grid row -->
      <div class="row mt-3">
        <!-- Grid column -->
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4" style="text-align:right;">
          <!-- Content -->
          <h5 class="text-uppercase fw-bold mb-4" style= "font-family: 'Cairo', sans-serif;">  
            <img  style="height: 35px; width: 35px;" src="{{asset('assets/admin/1.png')}}" alt="">
            &nbsp;
            <a href="https://cloudszone.net/ar/clouds-zone-arabia-fo/">
             منطقة السحاب 
           </a>
          </h5>
          <p> 
          منطقتك التقنية الآمنة للعمل
          </p>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4"  style="text-align:right;">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4" style= "font-family: 'Cairo', sans-serif;">
            <a href="https://cloudszone.net/ar/%d8%ae%d8%af%d9%85%d8%a7%d8%aa-%d8%a7%d9%84%d8%aa%d9%8a-%d8%aa%d9%82%d8%af%d9%85%d9%87%d8%a7-%d8%b4%d8%b1%d9%83%d8%a9-%d9%85%d9%86%d8%b7%d9%82%d8%a9-%d8%a7%d9%84%d8%b3%d8%ad%d8%a7%d8%a8/">
            خدماتنا
          </a>
          </h6>
          <p>
            <a href="https://mystifying-cray.3-11-62-26.plesk.page/" class="text-decoration-none" style="color: #6c757d;">
              إرسال رسائل SMS
            </a>
          </p>
          <p>
          إرسال رسائل واتساب
            <!-- <a href="#!" class="text-reset"> </a> -->
          </p>
          
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <!-- <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4"> -->
          <!-- Links -->
          <!-- <h6 class="text-uppercase fw-bold mb-4">
            Useful links
          </h6>
          <p>
            <a href="#!" class="text-reset">Pricing</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Settings</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Orders</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Help</a>
          </p>
        </div> -->
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4"  style="text-align:right;">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4" style= "font-family: 'Cairo', sans-serif;">
          <a href="https://cloudszone.net/ar/%d8%aa%d9%88%d8%a7%d8%b5%d9%84-%d9%85%d8%b9%d9%86%d8%a7/">
            تواصل معنا
          </a>
          </h6>
          <p><i class="icon-home"></i>
            <a href="https://www.google.com/maps/place/Clouds+Zone+for+IT+Solutions+%D9%85%D9%86%D8%B7%D9%82%D8%A9+%D8%A7%D9%84%D8%B3%D8%AD%D8%A7%D8%A8+%D9%84%D9%84%D8%AA%D9%82%D9%86%D9%8A%D8%A9%E2%80%AD/@32.4609517,-0.3103753,4z/data=!4m5!3m4!1s0x15c3d1bdd867e1ef:0xdf0b2f2540dd717!8m2!3d21.54989!4d39.1943579?shorturl=1" class="text-decoration-none" style="color: #6c757d;">
            <span>شارع المكرونة</span>
            <p>جدة - المملكة العربية السعودية</p>
            </a>
          </p>

          <p> 
            <a href="mailto:info@cloudszone.net" class="text-decoration-none " style="color: #6c757d;">info@cloudszone.net</a>
            <i class="icon-envelope"></i>

          </p>

          <p> 
            <a href="mailto:Support@cloudszone.net" class="text-decoration-none" style="color: #6c757d;">Support@cloudszone.net</a>
            <i class="icon-envelope"></i>

          </p>
          <p>
          
          <i class="icon-screen-smartphone"></i>

          <label> +966548689528 </label>
        </p> 

        </div>
        <!-- Grid column -->
      </div>
      <!-- Grid row -->
    </div>
  </section>
  <!-- Section: Links  -->

  <!-- Copyright -->
  <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
    © 2021 Copyright:
    <a class="text-reset fw-bold" href="https://cloudszone.net/">cloudszone.net</a>
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->