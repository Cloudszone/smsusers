@extends('layouts.logout')
@section('title','welcome moderen curtain ')  
<!-- Contact Section -->

<Html lang="en">
<br xmlns="http://www.w3.org/1999/html">
<section class="flexbox-container">
    <div class="col-lg-12 col-md-12 d-flex align-items-center justify-content-center">
        <div class="col-lg-8 col-md-8 box-shadow-2 p-0">
            <div class="card border-grey border-lighten-3 m-0">
                <div class="card-header border-0">
                    <div class="card-title text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="card-body">
                                    @include('alerts.error')
                                    @include('alerts.success')

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>رسالتك</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-lg-offset-2">
                    <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                    <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                    <form name="sentMessage" id="contactForm"  METHOD="POST" action="{{route('send')}}">
                        @csrf
                        <!-- Line test<form name="sentMessage" id="contactForm" method="post" action="http://samuel.net46.net/toExternal/contact_me.php"> -->

                        <div class="row control-group ">
                            <div  class=" col-lg-3  form-group"></div>
                            <div class=" col-lg-6 col-md-12 form-group">
                                <label for="sel1"> الاسم</label>
                                <select class="form-control" id="" name="sender_name">
                                    <option value="CURTAINS">CURTAINS</option>
                                    <option value="CURTAINS-AD">CURTAINS-AD</option>
                                    <option value="MCSTORE">MCSTORE</option> 
                                    <option value="MCURTAIN-AD">MCURTAIN-AD</option> 
                                    <option value="MOD-CURTAIN">MOD-CURTAIN</option> 
                                </select>
                            </div>
                            <div  class=" col-lg-3   form-group"></div>
                        </div>
                        <div class="row control-group">
                            <div class="col-lg-6">
                                <div class="form-group"> 
                                    <label>الأرقام المرادالإرسال لها</label>
                                    <textarea rows="6" class="form-control" dir="ltr" title="الارقام المراد الارسال لها " name="mobiles[]" id="mobiles" required data-validation-required-message="Please enter  mobile numbers." ></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>

                            <div class="col-lg-6  ">
                                <div class="form-group"> 
                                    <label>رسالتك</label>
                                    <textarea rows="6" class="form-control" dir="ltr"  title="رسالتك" name="message" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            
                        </div>
                    
                        <br>
                        <div id="success"></div>
                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <button type="submit" name="submit" class="btn btn-info btn-lg">إرسال</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
 </div></div></div></div></div></div></div></div></section></Html>

                             