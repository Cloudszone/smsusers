<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use Auth;
use App\Models\User;
use App\Models\Subscriber;
use App\Models\Message;
use App\Models\SenderName;

class SenderController extends Controller
{
    public function sendcloud(Request $request){ 
        $user = Auth::User();
        $subscriber = $user->subscriber;

        $text=$request->message;
        $user = $subscriber->user_name;
        // $password = Crypt::decryptString($subscriber->password);
        $password = decrypt($subscriber->pwd);
        // return $password;
        // $encryptedPassword = encrypt($password);
        // $decryptedPassword = decrypt($encryptedPassword);
        $text = urlencode( $text);
        $sender_name = SenderName::find($request->sender_name_id);
        $sendername= $sender_name->name;

        foreach($request->mobiles as $mobile){ 
            $to = $mobile;
            $url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E&return=json"; 
            $ret = file_get_contents($url);
            $resulte = json_decode($ret); 
        } 
 
        //  Create Validation  ..
        if ( $resulte->Code==100){
        
            // Create Object From Message .. 
            $message = new Message ;
            $message->code = $resulte->Code;
            $message->message = $resulte->MessageIs;
            $message->valid_numbers = $resulte->valid;
            $message->nvalid_numbers = $resulte->nvalid;
            $message->blocked_numbers = $resulte->Blocked;
            $message->Repeated_numbers = $resulte->Repeated;
            $message->last_user_points = $resulte->lastuserpoints;
            $message->SMS_number = $resulte->SMSNUmber;
            $message->total_cout = $resulte->totalcout;
            $message->current_user_points = $resulte->currentuserpoints;
            $message->total_sent_numbers = $resulte->totalsentnumbers; 
            $user = Auth::User();
            $message->user_id  = $user->id;
            $message->save();

            return back()->with(['success' => 'تم استلام الرسالة بنجاح',
                                'currentuserpoints' => $resulte->currentuserpoints ]);
        }
        return $resulte;
        
        if ( $resulte->Code==101)
            return back()->with(['error' => 'المعلومات غير مكتملة']); 

        if ( $resulte->Code==102)
            return back()->with(['error' => '   اسم المستخدم غير صحيح  ']); 

        if ( $resulte->Code==103)
            return back()->with(['error' => 'كلمة السر غير صحيحة']); 

        if ( $resulte->Code==105)
            return back()->with(['error' => 'الاشتراك منتهي']);

        if ( $resulte->Code==106)
            return back()->with(['error' => 'اسم المرسِل غير موجود']);

        if ( $resulte->Code==107)
            return back()->with(['error' => 'اسم المرسِل محظور']); 
            
        if ( $resulte->Code==108)
            return back()->with(['error' => 'لا توجد أرقام صالحة لإرسالها']); 

        if ( $resulte->Code==109)
            return back()->with(['error' => 'لا يمكن الإرسال لأكثر من ثماني أرقام']); 

        if ( $resulte->Code==111)
            return back()->with(['error' => 'الإرسال غير مدعوم حالياً']);
            
        if ( $resulte->Code==112)
            return back()->with(['error' => 'يوجد كلمات محظورة لا تدعم الإرسال']); 

        if ( $resulte->Code==113)
            return back()->with(['error' => 'الحساب غير مفعل']); 

        if ( $resulte->Code==114)
            return back()->with(['error' => 'الحساب معطل']); 

        if ( $resulte->Code==115)
            return back()->with(['error' => 'رقم الموبايل غير مفعل']); 

        if ( $resulte->Code==116)
            return back()->with(['error' => 'الإيميل غير مفعل']); 

        if ( $resulte->Code==117)
            return back()->with(['error' => 'الرسالة فارغة']);

        if ( $resulte->Code==1015)
            return back()->with(['error' => 'لا يوجد اسم المرسِل']);
        
        if ( $resulte->Code==1013)
            return back()->with(['error' => 'لا يمكن إرسال رسالة فارغة']);
            
        return back()->with(['error' => 'لم يتم إرسال الرسالة']);  
    }


    public function sendcurtain(Request $request){
  
        $text=$request->message;
        $user = 'moderncurtain';
        $password = 'Moderncurtain'; 
        
        $text = urlencode( $text); 
        $sendername= $request->sender_name; 
       
        foreach($request->mobiles as $mobile){
            $to=$mobile; 
            $url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E&return=json"; 
            $ret = file_get_contents($url);
            $resulte = json_decode($ret); 
        } 

        // Create Object From Message .. 
            $message->code = $resulte->Code;
            $message->message = $resulte->MessageIs;
            $message->valid_numbers = $resulte->valid;
            $message->nvalid_numbers = $resulte->nvalid;
            $message->blocked_numbers = $resulte->Blocked;
            $message->Repeated_numbers = $resulte->Repeated;
            $message->last_user_points = $resulte->lastuserpoints;
            $message->SMS_number = $resulte->SMSNUmber;
            $message->total_cout = $resulte->totalcout;
            $message->current_user_points = $resulte->currentuserpoints;
            $message->total_sent_numbers = $resulte->totalsentnumbers; 
            $user = Auth::User();
            $message->user_id  = $user->id;
            $message->save();
        
        //  Create Validation  .. 
        if ( $resulte->Code==100)
            return back()->with(['success' => 'تم استلام الرسالة بنجاح',
                                'currentuserpoints' => $resulte->currentuserpoints ]);
        
        if ( $resulte->Code==101)
            return back()->with(['error' => 'المعلومات غير مكتملة']); 

        if ( $resulte->Code==102)
            return back()->with(['error' => '   اسم المستخدم غير صحيح  ']); 

        if ( $resulte->Code==103)
            return back()->with(['error' => 'كلمة السر غير صحيحة']); 

        if ( $resulte->Code==105)
            return back()->with(['error' => 'الاشتراك منتهي']);

        if ( $resulte->Code==106)
            return back()->with(['error' => 'اسم المرسِل غير موجود']);

        if ( $resulte->Code==107)
            return back()->with(['error' => 'اسم المرسِل محظور']); 
            
        if ( $resulte->Code==108)
            return back()->with(['error' => 'لا توجد أرقام صالحة لإرسالها']); 

        if ( $resulte->Code==109)
            return back()->with(['error' => 'لا يمكن الإرسال لأكثر من ثماني أرقام']); 

        if ( $resulte->Code==111)
            return back()->with(['error' => 'الإرسال غير مدعوم حالياً']);
            
        if ( $resulte->Code==112)
            return back()->with(['error' => 'يوجد كلمات محظورة لا تدعم الإرسال']); 

        if ( $resulte->Code==113)
            return back()->with(['error' => 'الحساب غير مفعل']); 

        if ( $resulte->Code==114)
            return back()->with(['error' => 'الحساب معطل']); 

        if ( $resulte->Code==115)
            return back()->with(['error' => 'رقم الموبايل غير مفعل']); 

        if ( $resulte->Code==116)
            return back()->with(['error' => 'الإيميل غير مفعل']); 

        if ( $resulte->Code==117)
            return back()->with(['error' => 'الرسالة فارغة']);

        if ( $resulte->Code==1015)
            return back()->with(['error' => 'لا يوجد اسم المرسِل']);
        
        if ( $resulte->Code==1013)
            return back()->with(['error' => 'لا يمكن إرسال رسالة فارغة']);
            
        return back()->with(['error' => 'لم يتم إرسال الرسالة']);
    }
}
