<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::User();
        // return view('cloudszone' );
      
        if($user->is_active == 1){
            $subscriber = $user->subscriber;
            // return $subscriber;
            $senders_names = $subscriber->senders_names;
            $last_message = $user->messages()->orderBy('id', 'DESC')->first();

            if(isset($last_message)){
                return view('cloudszone' , compact('last_message' , 'user' , 'senders_names'));
            }
        return view('cloudszone' , compact('user' ,'senders_names'));
        }
 
        return redirect()->back()->with(['error' => 'هناك خطا بالبيانات']);
    }

    public function logout()
    {

        $gaurd = $this->getGaurd();
        $gaurd->logout();

        return redirect()->route('login');
    }

    private function getGaurd()
    {
        return auth('web');
    }
    

  
}
