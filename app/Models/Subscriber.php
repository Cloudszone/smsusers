<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    use HasFactory;
    
    public function senders_names(){
        return $this->hasmany('App\Models\SenderName','subscriber_id');
    }
}
